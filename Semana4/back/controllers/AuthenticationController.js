var userService = require('../services/UserService');
var express = require('express');
var app = express();

app.post('/logout', (req, res) => {
  let user = userService.findById(req.body.id);
  if(user.logged) {
    delete user.logged;
    userService.replace(user.id, user);
  }
  res.send({
    "message": "Sesión cerrada correctamente"
  });
});

app.post('/login', (req, res) => {
  let { email, password } = req.body;
  res.send(userService.authenticate(email, password));
});

module.exports = app;