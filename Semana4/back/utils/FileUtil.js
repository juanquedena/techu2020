exports.write = function(data, fileName) {
  let { ApiError } = require('../middlewares/ErrorHandler');
  let fs = require("fs");
  let jsonStringify = JSON.stringify(data);
  fs.writeFile(__dirname + '/../data/' + fileName, jsonStringify, "utf8", (err) =>  {
    if(err) {
      console.log(err);
      throw new ApiError(500, "Error al actualizar");
    }
  });
}