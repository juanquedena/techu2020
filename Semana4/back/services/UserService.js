const USER_ID = "user_id";

var { ApiError } = require('../middlewares/ErrorHandler');
var fileUtil = require('../utils/FileUtil');
var arrayUtil = require('../utils/ArrayUtil');
var users = require('../data/users.json');

function find(page, size){
  return null;
}

function findById(id){
  let user = arrayUtil.findOneById(users, USER_ID, id).data;
  if(user === null) {
    throw new ApiError(409, "Identificador inválido");
  }
  return user;
}

function authenticate(email, password){
  let { pos, data } = arrayUtil.findOneById(users, "email", email);
  if(data == null) {
    throw new ApiError(409, "Correo electrónico inválido o no registrado");
  }
  if(data.password === password) {
    data.logged = true;
    fileUtil.write(users, "users.json");
  } else {
    throw new ApiError(409, "Contraseña inválida");
  }

  return {
    "message": "Usuario autentificado",
    "id": data.user_id,
    "token": ""
  };
}

function _update(id, callback){
  let { pos, data } = arrayUtil.findOneById(users, USER_ID, id);
  if(data === undefined) {
    throw new ApiError(409, "Identificador inválido");
  }
  callback(pos, data);
  fileUtil.write(users, "users.json");
}

function update(id, user){
  _update(id, (pos, data) => {
    for(var prop in user) {
      data[prop] = user[prop];
    }
  });
}

function replace(id, user){
  _update(id, (pos, data) => {
    users[pos] = { ...user };
    users[pos][USER_ID] = id;
  });
}

module.exports = {
  find,
  findById,
  authenticate,
  update,
  replace
}