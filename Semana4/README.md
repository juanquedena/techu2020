![Tech U](http://icons.iconarchive.com/icons/kalangozilla/skrynium-black/72/transformers-decepticons-icon.png)

# Tech U 2020 - Particioner #
## Proyecto de la semana 4 ##

Implementando request-json como cliente para el consumo del api de mlab

## URI de los servicios ##

### Users ###
* GET: users
* GET: users/{user_id}
* POST: users
* PUT: users/{user_id}
* PATCH: users/{user_id}
* DELETE users/{user_id}

### Users / Accounts ###
* GET: users/{user_id}/accounts
* GET: users/{user_id}/accounts/{account_id}
* POST: users/{user_id}/accounts
* PUT: users/{user_id}/accounts/{account_id}
* PATCH: users/{user_id}/accounts/{account_id}
* DELETE: users/{user_id}/accounts/{account_id}

### Transactions ###
* GET: transactions
* GET: transactions/{transaction_id}
* POST: transactions
* PUT: transactions/{transaction_id}
* PATCH: transactions/{transaction_id}
* DELETE: transactions/{transaction_id}

## Postman Test  ##

Para probar en postman, importar el environment y la collection de pruebas
```
01-Semana4.postman_collection.json
Local.postman_environment.json
```

## Responsable ##
* Juan Quedena juanquedena@gmail.com
