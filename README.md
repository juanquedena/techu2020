![Tech U](http://icons.iconarchive.com/icons/kalangozilla/skrynium-black/72/transformers-decepticons-icon.png)

# Tech U 2020 - Particioner #

## Resumen ##
Taller practico sobre visión market

## Enlaces de Interes ##

## Proyectos ##
* Semana 1 - Pendiente
* [Semana 2](https://bitbucket.org/juanquedena/techu2020/src/master/Semana2/)
* [Semana 3](https://bitbucket.org/juanquedena/techu2020/src/master/Semana3/)
* [Semana 4](https://bitbucket.org/juanquedena/techu2020/src/master/Semana4/)
* [Semana 5](https://bitbucket.org/juanquedena/techu2020/src/master/Semana5/)

## Responsable ##
* Juan Quedena juanquedena@gmail.com
