/*
 * Nombre de Usuario: techX
 * Contraseña: techU2019
 * Base de Datos: techXdb
 */

const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const PORT = 4000
const URL_BASE = '/api-peru/v1/'

var userFile = require('./user.json')

const next = () => {
    [ ...userFile ].sort((a, b) => a.id == b.id ? 0 : (a.id > b.id ? -1 : 1))
    return userFile[0].id + 1
}

const find = query => {
    return userFile.filter(e => e.id == query.id)
}

const findDelete = id => {
    for(var i in userFile) {
        if(userFile[i].id == id) {
            return i
        }
    }
    return -1
}

app.use(bodyParser.json())

app.get(URL_BASE + 'users_count', (request, response) => {
    // response.send(JSON.stringify({ num_elem: userFile.length }))
    // response.json({ num_elem: userFile.length })
    response.send({ "num_elem": userFile.length })
})

app.get(URL_BASE + 'users', (request, response) => {
    response.status(200)
    console.log(request.query)
    let max = request.query.max || "-1";
    if(max == "-1")
        response.send(userFile)
    else {
        let users = []
        for(var i = 0; i < max; i++) {
            users.push(userFile[i])
        }
        response.send(users)
    }
})

app.get(URL_BASE + 'users/:id', (request, response) => {
    let user = find({ "id": request.params.id })
    if(user.length == 0)
        response.status(204).send()
    else
        response.send(user)
})

app.post(URL_BASE + 'users', (request, response) => {
    let user = {
        "id": next(),
        "first_name": request.body.first_name,
        "last_name": request.body.last_name,
        "email": request.body.email,
        "password": request.body.password
    }
    userFile.push(user)
    response.status(201).send({
        "message": "Usuario creado correctamente",
        "user": user
    })
})

app.put(URL_BASE + 'users/:id', (request, response) => {
    let id = request.params.id
    let users = find({ id })
    if(users.length == 0) {
        response.status(204).send()
    } else {
        let user = users[0]
        user.first_name = request.body.first_name
        user.last_name = request.body.last_name
        user.email = request.body.email
        user.password = request.body.password
    
        response.status(202).send({
            "message": "Usuario modificado correctamente",
            "user": user
        })
    }
})

app.delete(URL_BASE + 'users/:id', (request, response) => {
    let id = request.params.id
    let index = findDelete(id)
    if(index == -1) {
        response.status(204).send()
    } else {
        let user = userFile[index]
        userFile.splice(index, 1)
    
        response.status(202).send({
            "message": "Usuario eliminado correctamente",
            "user": user
        })
    }
})

app.listen(PORT, () => {
    console.log("Escuchando en el puerto " + PORT)
})