class ArrayUtils {

  static nextId(data, fieldId) {
    let dataTemp = [ ...data ];
    dataTemp.sort((a, b) => a[fieldId] === b[fieldId] ? 0 : (a[fieldId] > b[fieldId] ? -1 : 1));
    return dataTemp[0][fieldId] + 1;
  }
  
  static findById(data, fieldId, value) {
    return data.filter(e => e[fieldId] === value);
  }
  
  static findOneById(data, fieldId, value) {
    let elem = {};
    let exists = data.some((e, index) => {
      elem.pos = index;
      elem.data = e;
      return e[fieldId] === value;
    });
    return exists ? elem : { pos: -1, data: null };
  }
}

module.exports = ArrayUtils;