const PORT = process.env.PORT || 4000;
const URL_BASE = '/api-peru/v1/';

const express = require('express');
const bodyParser = require('body-parser');
const { errorHandler } = require('./middlewares/ErrorHandler');
const { corsHandler } = require('./middlewares/CorsHandler');

const authenticationController = require('./controllers/AuthenticationController');
const userController = require('./controllers/UserController');

var app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(corsHandler);
app.use(URL_BASE, authenticationController);
app.use(URL_BASE, userController);
app.use(errorHandler);

app.listen(PORT, () => {
  console.log("Escuchando en el puerto " + PORT);
});