class ApiError extends Error {

  constructor(status, message) {
    super(message);
    this.status = status;
    this.name = 'ApiError';
    Error.captureStackTrace(this, ApiError);
  }
}

function errorHandler(err, req, res, next) {
  if (res.headersSent) {
    return next(err);
  } else if(err instanceof ApiError) {
    res.status(err.status).json({ error: err, message: err.message });
  } else {
    res.status(500).json({ error: err, message: err.message });
  }
}

module.exports = { errorHandler, ApiError }