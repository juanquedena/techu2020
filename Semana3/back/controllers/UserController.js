var userService = require('../services/UserService');
var express = require('express');
var app = express();

app.get('/users', (req, res) => {
  res.send(userService.find());
});

app.get('/users/:id', (req, res) => {
  res.send(userService.findById(parseInt(req.params.id)));
});

app.post('/users', (req, res) => {
  res.send(userService.find());
});

app.put('/users/:id', (req, res) => {
  res.send(userService.find());
});

app.patch('/users/:id', (req, res) => {
  res.send(userService.find());
});

module.exports = app;