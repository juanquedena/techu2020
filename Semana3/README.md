![Tech U](http://icons.iconarchive.com/icons/kalangozilla/skrynium-black/72/transformers-decepticons-icon.png)

# Tech U 2020 - Particioner #
## Proyecto de la semana 3 ##

Implementando login - logout

Para probar en postman, importar el environment y la collection de pruebas
```
01-Semana3.postman_collection.json
Local.postman_environment.json
```

## Responsable ##
* Juan Quedena juanquedena@gmail.com
